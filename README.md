## PHP_CodeSniffer

```sh
$ cd ~ && curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar && sudo chmod a+x phpcs.phar && sudo ln -s `pwd`/phpcs.phar /usr/local/bin/phpcs
```

## PHP Code Beautifier and Fixer

```sh
$ cd ~ && curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar && sudo chmod a+x phpcbf.phar && sudo ln -s `pwd`/phpcbf.phar /usr/local/bin/phpcbf
```

## PHP CS Fixer

```sh
$ cd ~ && curl -OL https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/download/v1.11.6/php-cs-fixer.phar && sudo chmod a+x php-cs-fixer.phar && sudo ln -s `pwd`/php-cs-fixer.phar /usr/local/bin/php-cs-fixer
```

## PHP Mess Detector

```sh
$ cd ~ && curl -OL https://phpmd.org/static/latest/phpmd.phar && sudo chmod +x phpmd.phar && sudo ln -s `pwd`/phpmd.phar /usr/local/bin/phpmd
```

## Phpcs no sublime
> `ctrl + shift + p` > `Install Package` > `Phpcs`
>
> `Preferences` > `Package Settings` > `PHP Code Sniffer` > `Settings - User`
>
> [Cole este conte�do](https://bitbucket.org/otaavioo/sublime/raw/7570e6a5d7882e256248361405b6881f0822a7d0/phpcs.sublime-settings)